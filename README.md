# Pokedex

[![pipeline status](https://gitlab.com/Aleksandr909/pika-pika/badges/master/pipeline.svg)](https://gitlab.com/Aleksandr909/pika-pika/-/commits/master)

### [Приложение](https://pika.vorfolio.ru/)

**Fullstack приложение для:**

1. Регистрации по почте или через соц. сети и просмотра профиля.
1. Просмотра покемонов с поиском по типу, имени (также сделан голосовой поиск);
1. Добавления понравившихся в избранное и их просмотра;

### Используемые технологии

**Frontend:**

- React;
- React router;
- React hook form;
- Mobx;
- Mobx react lite;
- Material-ui;
- Typescript;
- Axios.

**Backend:**

- Node.js;
- Express;
- MongoDB;
- Mongoose;
- Jsonwebtoken;
- Bcrypt;
- Passport;
- Typescript;
- Axios.

Проект размещен на сервере Vscale с Ubuntu OS. Настроено https (с помощью **letsencrypt** создан ssl сертификат). С помощью **[gitlab ci](https://gitlab.com/Aleksandr909/pika-pika/-/blob/master/.gitlab-ci.yml)** проект автоматически собирается на серверах gitlab и затем переносится на сервер. На сервере с помощью **nginx** приложение проксируется с порта на нужный поддомен.

### Локальное тестирование

```sh
git clone https://gitlab.com/Aleksandr909/pika-pika.git
cd pika-pika
npm run load
npm run dev
```

### Лицензия

[BSD 2-clause "Simplified" License](https://gitlab.com/Aleksandr909/pika-pika/-/blob/master/LICENSE)
