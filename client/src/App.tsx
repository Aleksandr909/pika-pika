import { createMuiTheme, ThemeProvider } from '@material-ui/core'
import CssBaseline from '@material-ui/core/CssBaseline'
import axios from 'axios'
import React from 'react'
import { Router as BrowserRouter } from 'react-router-dom'
import { API_PATH_MIN } from './constants/path'
import history from './history'
import Router from './Router'
import Header from './ui/organisms/Layout'
import 'mobx-react-lite/batchingForReactDom'

axios.defaults.baseURL = API_PATH_MIN
axios.defaults.headers.post['Content-Type'] = 'application/json'

const theme = createMuiTheme({
  palette: {
    primary: { main: '#F57F17', contrastText: '#FFFDE7' },
  },
})

const App = () => (
  <ThemeProvider theme={theme}>
    <BrowserRouter history={history}>
      <CssBaseline />
      <Header>
        <Router />
      </Header>
    </BrowserRouter>
  </ThemeProvider>
)

export default App
