import React from 'react'
import { Route, Switch } from 'react-router-dom'
import AuthPage from './pages/AuthPage'
import FavoritePage from './pages/FavoritePage'
import AuthSuccess from './pages/AuthSuccess'
import HomePage from './pages/HomePage'
import PokemonsPage from './pages/PokemonsPage'
import Profile from './pages/ProfilePage'
import SignUpPage from './pages/SignUpPage'
import PrivateRoute from './ui/molecules/PrivateRoute'
import AuthRoute from './ui/molecules/AuthRoute'

const Router = () => (
  <Switch>
    <Route exact path="/">
      <HomePage />
    </Route>
    <Route exact path="/pokemons">
      <PokemonsPage />
    </Route>
    <AuthRoute exact path="/sign-in">
      <AuthPage />
    </AuthRoute>
    <PrivateRoute exact path="/sign-up">
      <SignUpPage />
    </PrivateRoute>
    <PrivateRoute exact path="/profile">
      <Profile />
    </PrivateRoute>
    <PrivateRoute exact path="/favorite">
      <FavoritePage />
    </PrivateRoute>
    <Route exact path="/auth/google/:token">
      <AuthSuccess />
    </Route>
    <Route exact path="/auth/facebook/:token">
      <AuthSuccess />
    </Route>
  </Switch>
)

export default Router
