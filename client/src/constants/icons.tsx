import BACKGROUND_IMAGE from '../img/background_image.jpg'
import { ReactComponent as Google } from '../img/google-ico.svg'
import LOGO from '../img/logo.png'

export { BACKGROUND_IMAGE, LOGO, Google }
