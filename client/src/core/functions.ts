const colors = {
  normal: '#FF8F00',
  fighting: '#311B92',
  flying: '#00B0FF',
  poison: '#00E676',
  ground: '#6D4C41',
  rock: '#3E2723',
  bug: '#D84315',
  ghost: '#757575',
  steel: '#424242',
  fire: '#D50000',
  water: '#2979FF',
  grass: '#388E3C',
  electric: '#00E5FF',
  psychic: '#1A237E',
  ice: '#3D5AFE',
  dragon: '#00838F',
  dark: '#263238',
  fairy: '#C2185B',
  unknown: '#546E7A',
  shadow: '#616161',
}

export const getColor = (name: keyof typeof colors) => colors[name]

export const getParams = ({
  page, size, type, name,
}: { page?: number; size?: number, type: string, name: string }) => {
  let params = '?'
  params += (!!page && `page=${page}&`) || ''
  params += (!!size && `size=${size}&`) || ''
  params += (!!type && `type=${type}&`) || ''
  params += (!!name && `name=${name}`) || ''
  return encodeURI(params)
}
