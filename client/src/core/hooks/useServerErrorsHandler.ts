import { AxiosError } from 'axios'
import { useEffect } from 'react'
import { ErrorOption } from 'react-hook-form/dist/types/form'

/**
 * Хук для работы с ошибками с бэка
 *
 * serverErrors - ошибка с сервера
 *
 * setError - из хука react-hook-form
 *
 * Автоматически меняет Ошибки полей при изменении serverErrors
 *
 * Изпользовать: useServerErrorsHandler(serverErrors, setError)
 *
 * @param serverErrors Ошибки с сервера
 * @param setError Из хука форм для смены ошибок полей
 */
const useServerErrorsHandler = (
  serverErrors: AxiosError | null,
  setError: (name: any, error: ErrorOption) => void,
) => {
  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    serverErrors?.response?.data
      && Object.keys(serverErrors.response.data).forEach((eKey: string) => {
        setError(eKey, { type: 'serverError', message: serverErrors.response?.data[eKey] })
      })
  }, [serverErrors, setError])
}
export default useServerErrorsHandler
