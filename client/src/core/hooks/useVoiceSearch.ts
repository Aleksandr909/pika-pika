import { useEffect } from 'react'

const useVoiceSearch = (classes:{[key:string]: any}, onChange: (e:any) => void) => {
  useEffect(() => {
    const $voiceTrigger = document.querySelector('#voice-trigger')
    const $searchInput = document.querySelector('#search-field')
    const inputClassName = $searchInput?.className || ''

    window.SpeechRecognition = window.SpeechRecognition || (window as any).webkitSpeechRecognition
    const { SpeechRecognition } = window
    const recognition = new SpeechRecognition()

    if (window.SpeechRecognition) {
      recognition.interimResults = true
      recognition.lang = 'en-EN'
      recognition.addEventListener('result', _transcriptHandler)
      recognition.addEventListener('speechend', listenEnd)
    } else {
      $voiceTrigger?.remove()
    }
    $voiceTrigger?.addEventListener('click', listenStart)

    function listenStart(e:any) {
      e.preventDefault()
      $searchInput?.setAttribute('disabled', 'true')
      $searchInput?.setAttribute('placeholder', 'Speak…')
      if ($voiceTrigger) $voiceTrigger.className = `${classes.searchIcon} ${classes.disabled}`
      if ($searchInput) $searchInput.className = `${inputClassName} Mui-disabled`
      recognition.start()
    }

    function _parseTranscript(e:any) {
      return Array.from(e.results).map((result) => (result as any)[0]).map((result) => result.transcript).join('')
    }

    function _transcriptHandler(e:any) {
      const speechOutput = _parseTranscript(e)
      if ($searchInput) $searchInput.nodeValue = speechOutput
      if (e.results[0].isFinal) {
        onChange({ target: { value: speechOutput } })
      } else {
        onChange({ target: { value: '' } })
      }
    }
    function listenEnd() {
      if ($voiceTrigger) $voiceTrigger.className = classes.searchIcon
      if ($searchInput) $searchInput.className = inputClassName
      $searchInput?.removeAttribute('disabled')
      $searchInput?.setAttribute('placeholder', 'Search…')
    }
    // eslint-disable-next-line
  }, [])
}
export default useVoiceSearch
