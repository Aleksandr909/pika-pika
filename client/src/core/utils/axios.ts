import axios, { AxiosError } from 'axios'
import { API_PATH } from '../../constants/path'
import {
  getValueFromLocalStorage,
} from './localStorage'

const apiEndpoint = API_PATH
const axiosInstance = axios.create({
  baseURL: apiEndpoint,
})

axiosInstance.interceptors.request.use((config) => {
  const accessToken = getValueFromLocalStorage('token')

  return {
    ...config,
    data: config.data,
    headers: {
      ...config.headers,
      ...{ Authorization: `Bearer ${accessToken}` },
    },
  }
})

axiosInstance.interceptors.response.use(
  (response) => ({
    ...response,
    data: response.data,
  }),
  async (error: AxiosError) => Promise.reject(error),
)

export default axiosInstance
