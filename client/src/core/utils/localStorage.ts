export const getValueFromLocalStorage = <T>(key: string) => {
  const value = localStorage.getItem(key)
  const result: T | null = value ? JSON.parse(value) : value

  return result
}

export const setValueToLocalStorage = <T>(key: string, value: T) => {
  try {
    localStorage.setItem(key, JSON.stringify(value))
  } catch (error) {
    return false
  }
}

export const removeValueFromLocalStorage = (key: string) => {
  localStorage.removeItem(key)
}

export const clearLocalStorage = () => {
  localStorage.clear()
}
