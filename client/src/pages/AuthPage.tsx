import Container from '@material-ui/core/Container'
import React from 'react'
import AuthForm from '../ui/molecules/AuthForm'

const AuthPage = () => (
  <Container fixed>
    <AuthForm />
  </Container>
)

export default AuthPage
