import { observer } from 'mobx-react-lite'
import React, { useEffect } from 'react'
import { useRouteMatch } from 'react-router-dom'
import { setValueToLocalStorage } from '../core/utils/localStorage'
import UsersStore from '../stores/UsersStore'
import CustomProgress from '../ui/atoms/CustomProgress'
import history from '../history'

const AuthSuccess = observer(() => {
  const { token } = useRouteMatch().params as { token: string }
  const { getUserSelf, currentUser } = React.useContext(UsersStore)
  useEffect(() => {
    setValueToLocalStorage('token', token)
    getUserSelf()
  }, [getUserSelf, token])
  useEffect(() => {
    if (currentUser) history.push('/profile')
  }, [currentUser])

  return (
    <CustomProgress />
  )
})

export default AuthSuccess
