import Container from '@material-ui/core/Container'
import * as React from 'react'
import Favorite from '../ui/organisms/Favorite'

type IFavoritePageProps = {
}

const FavoritePage: React.FunctionComponent<IFavoritePageProps> = (p) => (
  <Container fixed>
    <Favorite />
  </Container>
)

export default FavoritePage
