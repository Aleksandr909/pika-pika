import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import * as React from 'react'
import { BACKGROUND_IMAGE } from '../constants/icons'
import LinkCustom from '../ui/atoms/LinkCustom'

interface IHomePageProps { }
const HomePage: React.FunctionComponent<IHomePageProps> = (props) => {
  const classes = useStyles()
  return (
    <div className={classes.img}>
      <Container fixed>
        <Grid container direction="column" justify="center" alignItems="flex-end" className={classes.main}>
          <Typography variant="h4" component="h1" gutterBottom>
            Pokedex
          </Typography>
          <LinkCustom
            to="/pokemons"
          >
            <Button color="primary" variant="contained">
              Начать
            </Button>
          </LinkCustom>
        </Grid>
      </Container>
    </div>
  )
}

const useStyles = makeStyles({
  img: {
    backgroundImage: `url(${BACKGROUND_IMAGE})`,
    backgroundSize: 'contain',
    height: '100vh',
    backgroundRepeat: 'no-repeat',
    position: 'fixed',
    top: 0,
    width: '100%',
  },
  main: {
    minHeight: '100vh',
  },
})

export default HomePage
