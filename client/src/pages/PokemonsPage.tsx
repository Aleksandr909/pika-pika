import Container from '@material-ui/core/Container'
import * as React from 'react'
import Pokemons from '../ui/organisms/Pokemons'

const PokemonsPage: React.FC = () => (
  <Container fixed>
    <Pokemons />
  </Container>
)

export default PokemonsPage
