import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import { observer } from 'mobx-react-lite'
import React from 'react'
import UsersStore from '../stores/UsersStore'
import ProfileForm from '../ui/molecules/ProfileForm'

const Profile = observer(() => {
  const { currentUser } = React.useContext(UsersStore)
  return (
    currentUser && (
      <Container fixed>
        <Box padding="24px 0">
          <ProfileForm />
        </Box>
      </Container>
    )
  )
})

export default Profile
