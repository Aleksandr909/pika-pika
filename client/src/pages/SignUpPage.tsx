import Container from '@material-ui/core/Container'
import React from 'react'
import SignUpForm from '../ui/molecules/SignUpForm'

const SignUpPage = () => (
  <Container fixed>
    <SignUpForm />
  </Container>
)

export default SignUpPage
