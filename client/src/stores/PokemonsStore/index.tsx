import axios from 'axios'
import { decorate, observable } from 'mobx'
import { createContext } from 'react'
import { getParams } from '../../core/functions'
import axiosInstance from '../../core/utils/axios'
import { Type } from '../TypesStore/types'
import { Pokemon, PokemonMini } from './types'

export class PokemonsStore {
  pokemons: null | Pokemon[] = null

  loading = false

  total = 0

  currentPokemon: null | Pokemon = null

  name: string = ''

  getFavorites = async (page: number, size: number, currentType: Type | null) => {
    try {
      this.loading = true
      const pokemons = await axiosInstance.get(`/user/favorite/${getParams(
        {
          page, size, type: currentType?.name || '', name: this.name,
        },
      )}`)

      this.pokemons = pokemons.data.results
      this.total = pokemons.data.count
      this.loading = false
    } catch {
      this.pokemons = null
      this.loading = false
      this.total = 0
    }
  }

  getPokemons = async (page: number, size: number, currentType: Type | null) => {
    try {
      this.loading = true
      const pokemonsFull = []
      if (currentType && this.name) {
        const pokemons = await axios.get(`/type/${currentType.name}`)
        const pokemonMini = pokemons.data.pokemon.find(
          ({ pokemon }: { pokemon: PokemonMini }) => pokemon.name === this.name,
        )
        if (pokemonMini) {
          const pokemonFull = await axios.get(`/pokemon/${pokemonMini.pokemon.name}`)
          pokemonsFull.push(pokemonFull.data)
          this.total = 1
        }
      } else if (currentType) {
        const pokemons = await axios.get(`/type/${currentType.name}`)
        const pokemonsNeeded: { pokemon: PokemonMini }[] = pokemons.data.pokemon.splice(
          page * size, (page + 1) * size,
        )
        for (const pokemon of pokemonsNeeded) {
          const pokemonFull = await axios.get(pokemon.pokemon.url)
          pokemonsFull.push(pokemonFull.data)
        }
        this.total = pokemons.data.pokemon.length
      } else if (this.name) {
        const pokemons = await axios.get(`/pokemon/${this.name}`)
        pokemonsFull.push(pokemons.data)
        this.total = 1
      } else {
        const pokemons = await axios.get(`/pokemon/?limit=${size}&offset=${page * size}`)
        for (const pokemon of pokemons.data.results as PokemonMini[]) {
          const pokemonFull = await axios.get(pokemon.url)
          pokemonsFull.push(pokemonFull.data)
        }
        this.total = pokemons.data.count
      }
      this.pokemons = pokemonsFull
      this.loading = false
    } catch {
      this.pokemons = null
      this.loading = false
      this.total = 0
    }
  }

  setName = (name: string) => {
    this.name = name
  }

  changeCurrentPokemon = (currentPokemon: Pokemon | null) => {
    this.currentPokemon = currentPokemon
  }
}

decorate(PokemonsStore, {
  pokemons: observable,
  loading: observable,
  total: observable,
  currentPokemon: observable,
  name: observable,
})

export default createContext(new PokemonsStore())
