export interface PokemonMini {
    name: string;
    url: string
}
export interface Pokemon {
    abilities: any[];
    base_experience: number;
    forms: any[];
    game_indices: any[];
    height: number;
    held_items: any[];
    id: number;
    is_default: true;
    location_area_encounters: string;
    moves: any[];
    name: string;
    order: number;
    species: { [key: string]: any };
    sprites: { [key: string]: any };
    stats: any[];
    types: any[];
    weight: number;
}
