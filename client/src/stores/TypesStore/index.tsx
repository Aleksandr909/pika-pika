import axios from 'axios'
import { decorate, observable } from 'mobx'
import { createContext } from 'react'
import { Type } from './types'

export class TypeStore {
  types: null | Type[] = null

  loading = false

  currentType: null | Type = null

  getTypes = async () => {
    try {
      this.loading = true
      const types = await axios.get('/type')

      this.types = types.data.results
      this.loading = false
    } catch {
      this.types = null
      this.loading = false
    }
  }

  changeCurrentType = (currentType: Type | null) => {
    this.currentType = currentType
  }
}

decorate(TypeStore, {
  types: observable,
  loading: observable,
  currentType: observable,
})

export default createContext(new TypeStore())
