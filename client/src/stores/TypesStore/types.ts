import { Pokemon } from '../PokemonsStore/types'

export interface TypeMini {
    name: string;
    url: string
}

export interface Type {
    damage_relations: { [key: string]: any };
    game_indices: any[];
    generation: { [key: string]: any };
    id: number;
    move_damage_class: { [key: string]: any };
    moves: any[];
    name: string;
    names: any[];
    pokemon: Pokemon[];
}
