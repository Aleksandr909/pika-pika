import { AxiosError } from 'axios'
import { decorate, observable } from 'mobx'
import { createContext } from 'react'
import axiosInstance from '../../core/utils/axios'
import { getValueFromLocalStorage, removeValueFromLocalStorage, setValueToLocalStorage } from '../../core/utils/localStorage'
import history from '../../history'
import { User } from './types'

export class UserStore {
  loading = true

  currentUser: null | User = null

  authSuccess = false

  user: null | User = null

  serverErrors: AxiosError | null = null

  isSuccess = false

  logoutSuccess = false

  getUserSelf = async () => {
    try {
      this.loading = true
      const token = getValueFromLocalStorage('token')
      if (token) {
        const user = await axiosInstance.get('/user/user-self')
        this.currentUser = user.data
      }
      this.loading = false
    } catch {
      this.loading = false
      removeValueFromLocalStorage('token')
    }
  }

  createUser = async (data: User) => {
    try {
      this.loading = true
      const user = await axiosInstance.post('/auth/register', data)
      setValueToLocalStorage('token', user.data.token)
      history.push('/')
      this.currentUser = user.data
      this.loading = false
      this.serverErrors = null
    } catch (error) {
      this.loading = false
      this.serverErrors = error
    }
  }

  login = async (data: Pick<User, 'email' | 'password'>) => {
    try {
      this.loading = true
      this.authSuccess = false
      const user = await axiosInstance.post('/auth/login', data)
      setValueToLocalStorage('token', user.data.token)
      this.currentUser = user.data
      this.authSuccess = true
      this.loading = false
      this.serverErrors = null
      this.authSuccess = false
    } catch (error) {
      removeValueFromLocalStorage('token')
      this.serverErrors = error
      this.loading = false
    }
  }

  pathcUser = async (data: User) => {
    try {
      this.loading = true
      const user = await axiosInstance.patch('/user/user-self', data)
      this.currentUser = user.data
      this.loading = false
      this.serverErrors = null
    } catch (error) {
      this.serverErrors = error
      this.loading = false
    }
  }

  changePassword = async (data: { old_password: string, new_password: string }) => {
    try {
      this.loading = true
      const user = await axiosInstance.patch('/user/change-password', data)
      this.currentUser = user.data
      this.loading = false
      this.isSuccess = true
      this.serverErrors = null
    } catch (error) {
      this.serverErrors = error
      this.loading = false
      this.isSuccess = false
    }
  }

  favoriteHandler = async (name: string) => {
    try {
      if (!this.currentUser) return
      this.loading = true
      await axiosInstance.patch('/user/favorite', { name })
      const itemIndex = this.currentUser.favorite.findIndex((elem) => elem === name)

      if (itemIndex !== -1) {
        this.currentUser.favorite.splice(itemIndex, 1)
      } else {
        this.currentUser.favorite.push(name)
      }
      this.loading = false
    } catch {
      this.loading = false
    }
  }

  logout = () => {
    this.logoutSuccess = true
    this.currentUser = null
    this.loading = false
    this.logoutSuccess = false
  }

  changeCurrentUser = (currentUser: User | null) => {
    this.currentUser = currentUser
  }
}

decorate(UserStore, {
  loading: observable,
  currentUser: observable,
  user: observable,
  serverErrors: observable,
  isSuccess: observable,
  authSuccess: observable,
  logoutSuccess: observable,
})

export default createContext(new UserStore())
