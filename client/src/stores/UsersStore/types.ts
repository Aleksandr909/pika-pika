export interface User {
    _id: string;
    email: string,
    avatar_path: string,
    password: string,
    last_name: string,
    middle_name: string,
    first_name: string,
    favorite: string[]
}
