import LinearProgress from '@material-ui/core/LinearProgress'
import { makeStyles } from '@material-ui/core/styles'
import * as React from 'react'

interface ICustomProgressProps {
}

const CustomProgress: React.FunctionComponent<ICustomProgressProps> = (props) => {
  const classes = useStyles()
  return <LinearProgress classes={{ root: classes.root }} />
}

const useStyles = makeStyles({
  root: {
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: '#A7FFEB',
    '& > .MuiLinearProgress-barColorPrimary': {
      backgroundColor: '#1DE9B6',
    },
  },
})

export default CustomProgress
