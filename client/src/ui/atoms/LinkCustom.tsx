import { makeStyles } from '@material-ui/core/styles'
import * as React from 'react'
import { Link, LinkProps } from 'react-router-dom'

const LinkCustom: React.FunctionComponent<LinkProps> = (
  { to, children }: LinkProps,
) => {
  const classes = useStyles()
  return (
    <Link to={to} className={classes.link}>
      {children}
    </Link>
  )
}

const useStyles = makeStyles({
  link: {
    color: 'inherit',
    textDecoration: 'none',
  },
})

export default LinkCustom
