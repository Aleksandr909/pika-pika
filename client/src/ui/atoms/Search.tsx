import InputBase from '@material-ui/core/InputBase'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import React from 'react'
import { Mic } from '@material-ui/icons'
import useVoiceSearch from '../../core/hooks/useVoiceSearch'

const useStyles = makeStyles((theme) => createStyles({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#eee',
    display: 'flex',
    '&:hover': {
      backgroundColor: '#F5F5F5',
    },
    margin: '8px 0 4px',
    height: 40,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  disabled: {
    pointerEvents: 'none',
    opacity: 0.8,
    cursor: 'auto',
  },
  inputRoot: {
    color: 'inherit',
    flex: 1,
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: 16,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 220,
    },
  },
}))

export default function Search(props: any) {
  const classes = useStyles()
  // eslint-disable-next-line
  useVoiceSearch(classes, props.onChange)

  return (
    <div className={classes.search}>
      <InputBase
        id="search-field"
        placeholder="Search…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
        {...props}
      />
      <div className={classes.searchIcon} id="voice-trigger">
        <Mic />
      </div>
    </div>
  )
}
