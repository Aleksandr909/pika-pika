import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { observer } from 'mobx-react-lite'
import React, { useEffect } from 'react'
import TypesStore from '../../stores/TypesStore'
import { Type } from '../../stores/TypesStore/types'

/**
 * Менюшка для выбора Юзера/ответственного/исполнителя
 */
const TypeAutocomplete = observer(({ TextFieldProps, ...props }: any) => {
  const {
    getTypes, types, loading, changeCurrentType, currentType,
  } = React.useContext(TypesStore)
  useEffect(() => {
    getTypes()
  }, [getTypes])
  const changeHandler = (e: any, value: Type | null) => {
    changeCurrentType(value)
  }

  if (!types || loading) {
    return (
      <TextField
        placeholder="Type"
        variant="outlined"
        disabled
        margin="dense"
        size="small"
      />
    )
  }
  if (types?.length === 0) {
    return (
      <TextField
        placeholder="Types not found"
        variant="outlined"
        disabled
        margin="dense"
        size="small"
      />
    )
  }
  return (
    <Autocomplete
      style={{ minWidth: 320, width: '100%' }}
      options={types}
      getOptionLabel={(option: Type) => option.name}
      ChipProps={{ size: 'small' }}
      disableCloseOnSelect
      getOptionSelected={(option: Type, value: Type) => value?.name === option?.name}
      {...props}
      renderOption={(option: Type) => option.name}
      onChange={changeHandler}
      value={currentType}
      renderInput={(params) => (
        <TextField
          {...TextFieldProps}
          {...params}
          label="Type"
          margin="dense"
          variant="outlined"
        />
      )}
    />
  )
})

export default TypeAutocomplete
