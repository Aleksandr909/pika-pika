import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Link from '@material-ui/core/Link'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { Facebook, Twitter } from '@material-ui/icons'
import { observer } from 'mobx-react-lite'
import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { Link as RouterLink, useLocation } from 'react-router-dom'
import { Google } from '../../constants/icons'
import { API_PATH } from '../../constants/path'
import useServerErrorsHandler from '../../core/hooks/useServerErrorsHandler'
import UsersStore from '../../stores/UsersStore'
import history from '../../history'

const AuthForm = observer(() => {
  const {
    login, serverErrors, loading, authSuccess,
  } = React.useContext(UsersStore)
  const location = useLocation().state as {from?: {[key:string]: any}}
  const historyPath = (location?.from as {pathname?:string})?.pathname

  const classes = useStyles()
  const {
    register, errors, handleSubmit, setError,
  } = useForm({
    defaultValues: { email: '', password: '' },
  })
  useServerErrorsHandler(serverErrors, setError)

  const onSubmit = handleSubmit((data) => {
    login(data)
  })

  useEffect(() => {
    if (authSuccess) history.push(historyPath || '/')
  }, [authSuccess, historyPath])

  return (
    <Grid container direction="column" alignItems="center" justify="center" className={classes.root} spacing={2}>
      <Grid item xs={12} sm={6} md={4} className={classes.authBlock}>
        <form onSubmit={onSubmit}>
          <TextField
            name="email"
            label="Email"
            variant="outlined"
            margin="dense"
            fullWidth
            inputRef={register({ required: true })}
            error={!!errors.email}
            helperText={errors.email?.message}
          />
          <TextField
            name="password"
            label="Password"
            variant="outlined"
            margin="dense"
            fullWidth
            type="password"
            inputRef={register({ required: true })}
            error={!!errors?.password}
            helperText={errors.password?.message}
          />
          <Button fullWidth color="primary" variant="contained" type="submit" disabled={loading}>
            Sign in
          </Button>
        </form>
      </Grid>
      <Grid item>
        <Grid container>
          <Typography variant="body1" color="textSecondary">
            Not registered?
          </Typography>
          <Link
            component={RouterLink}
            underline="always"
            to="/sign-up"
            className={classes.reg}
          >
            <Typography variant="body1">sign up</Typography>
          </Link>
        </Grid>
      </Grid>
      <Grid item>
        <Grid container>
          <Link href={`${API_PATH}/passport/google`}>
            <Google />
          </Link>
          <Link href={`${API_PATH}/passport/facebook`}>
            <Facebook className={classes.social} />
          </Link>
          <Twitter color="disabled" className={classes.social} />
        </Grid>
      </Grid>
    </Grid>
  )
})

const useStyles = makeStyles({
  root: { height: '100%', padding: 48 },
  authBlock: {
    padding: 24,
  },
  reg: { paddingLeft: 8 },
  social: { width: 50, height: 50 },
})

export default AuthForm
