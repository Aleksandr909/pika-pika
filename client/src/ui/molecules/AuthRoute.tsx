import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import { getValueFromLocalStorage } from '../../core/utils/localStorage'

type Props = {
  children: React.ReactNode;
  path: string;
  exact?: boolean
}

/**
 * Роут, проверяющий авторизован ли пользователь и если нет - перекидывает на страницу авторизации
 * @param children содержание роута для авторизировнных юзеров
 * @param path ссылка для авторизированных юзеров
 */
const AuthRoute = observer((props: Props) => {
  const token = getValueFromLocalStorage('token')

  return (
    <Route
      path={props.path}
      render={({ location }) => (!token ? (
        props.children
      ) : (
        <Redirect to={{ pathname: '/profile', state: { from: location } }} />
      ))}
    />
  )
})

export default AuthRoute
