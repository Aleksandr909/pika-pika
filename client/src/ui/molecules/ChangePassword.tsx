import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton'
import InputAdornment from '@material-ui/core/InputAdornment'
import useTheme from '@material-ui/core/styles/useTheme'
import TextField from '@material-ui/core/TextField'
import useMediaQuery from '@material-ui/core/useMediaQuery/useMediaQuery'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import { observer } from 'mobx-react-lite'
import React, { useEffect, useRef, useState } from 'react'
import { useForm } from 'react-hook-form'
import useServerErrorsHandler from '../../core/hooks/useServerErrorsHandler'
import UsersStore from '../../stores/UsersStore'

const ChangePassword = observer((): React.ReactElement => {
  const {
    changePassword, serverErrors, isSuccess, loading,
  } = React.useContext(UsersStore)
  const [open, setOpen] = React.useState(false)
  const theme = useTheme()
  const taskFormRef = useRef<HTMLFormElement>(null)
  const {
    register,
    errors,
    handleSubmit,
    watch,
    setError,
  } = useForm()
  useServerErrorsHandler(serverErrors, setError)
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))
  const newPassword = watch('new_password')
  const newPasswordRepeat = watch('new_password_repeat')

  const [showPassword, setShowPassword] = useState(false)
  const [showNewPassword, setShowNewPassword] = useState(false)
  const [showNewPasswordRepeat, setShowNewPasswordRepeat] = useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const onSubmit = handleSubmit((formData) => {
    const { new_password_repeat, ...data } = formData
    if (data.new_password === data.old_password) {
      handleClose()
      return
    }
    changePassword(data as { old_password: string; new_password: string; })
  })

  useEffect(() => {
    if (isSuccess) handleClose()
  }, [isSuccess])

  return (
    <>
      <Button
        variant="outlined"
        color="primary"
        fullWidth
        onClick={handleClickOpen}
      >
        Change password
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="dialog-title"
      >
        <DialogTitle id="dialog-title">
          Password changing
        </DialogTitle>
        <DialogContent>
          <form onSubmit={onSubmit} ref={taskFormRef}>
            <TextField
              id="old-password"
              name="old_password"
              type={showPassword ? 'text' : 'password'}
              placeholder="Old password"
              autoComplete="false"
              error={!!errors.old_password}
              helperText={errors.old_password?.message}
              fullWidth
              margin="dense"
              autoFocus
              variant="outlined"
              inputRef={register({ required: true, validate: (value) => getPasswordValid(value) })}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowPassword(!showPassword)}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              id="new-password"
              type={showNewPassword ? 'text' : 'password'}
              placeholder="New password"
              autoComplete="new-password"
              fullWidth
              margin="dense"
              error={!!errors.new_password}
              helperText={errors.new_password?.message}
              variant="outlined"
              name="new_password"
              inputRef={register(
                {
                  required: true,
                  validate: (value) => getPasswordValid(value) && value === newPasswordRepeat,
                },
              )}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowNewPassword(!showNewPassword)}
                    >
                      {showNewPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              id="new-password-repeat"
              type={showNewPasswordRepeat ? 'text' : 'password'}
              placeholder="New password repeat"
              fullWidth
              margin="dense"
              error={!!errors.new_password_repeat}
              helperText={errors.new_password_repeat?.message}
              variant="outlined"
              name="new_password_repeat"
              inputRef={register(
                {
                  required: true,
                  validate: (value) => getPasswordValid(value) && value === newPassword,
                },
              )}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowNewPasswordRepeat(!showNewPasswordRepeat)}
                    >
                      {showNewPasswordRepeat ? (
                        <Visibility />
                      ) : (
                        <VisibilityOff />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button onClick={onSubmit} color="primary" disabled={loading}>
            Change
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
})

const getPasswordValid = (password: string) => {
  const minRegex = /^[A-Za-z0-9]{6,}$/
  if (minRegex.test(password)) {
    return true
  }
  return false
}

export default ChangePassword
