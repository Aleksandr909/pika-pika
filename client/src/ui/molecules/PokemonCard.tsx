import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Checkbox from '@material-ui/core/Checkbox'
import Chip from '@material-ui/core/Chip'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Star from '@material-ui/icons/Star'
import StarBorder from '@material-ui/icons/StarBorder'
import Box from '@material-ui/core/Box'
import { observer } from 'mobx-react-lite'
import React from 'react'
import { getColor } from '../../core/functions'
import PokemonsStore from '../../stores/PokemonsStore'
import { Pokemon } from '../../stores/PokemonsStore/types'
import UsersStore from '../../stores/UsersStore'

interface IPokemonCardProps {
  pokemon: Pokemon
}

const useStyles = makeStyles({
  root: {
    position: 'relative',
    minWidth: 275,
    '&:hover': {
      boxShadow: '2px 2px 8px #616161',
      cursor: 'pointer',
    },
  },
  pos: {
    marginBottom: 12,
  },
  img: {
    objectFit: 'contain',
    maxWidth: '100%',
  },
  name: {
    textTransform: 'capitalize',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  chipBlock: {
    margin: '16px 0',
  },
  favorite: {
    position: 'absolute',
    right: 0,
  },
})

const PokemonCard = observer(({ pokemon }: IPokemonCardProps) => {
  const classes = useStyles()
  const { changeCurrentPokemon } = React.useContext(PokemonsStore)
  const { currentUser, favoriteHandler } = React.useContext(UsersStore)
  const bull = <span className={classes.bullet}>•</span>
  const isFavorite = !!currentUser?.favorite.includes(pokemon.name)
  return (
    <Card className={classes.root}>
      <Checkbox
        checked={isFavorite}
        onChange={() => favoriteHandler(pokemon.name)}
        checkedIcon={<Star />}
        icon={<StarBorder />}
        inputProps={{ 'aria-label': 'primary checkbox' }}
        className={classes.favorite}
      />
      <Box onClick={() => changeCurrentPokemon(pokemon)}>
        <CardContent>
          <img src={pokemon.sprites.front_default} alt={pokemon.name} className={classes.img} />
          <Typography variant="h5" component="h2" className={classes.name}>
            {pokemon.name}
          </Typography>
          <Grid container spacing={1} className={classes.chipBlock}>
            {pokemon.types.map(({ type }) => (
              <Grid item key={type.name}>
                <Chip label={type.name} size="small" style={{ backgroundColor: getColor(type.name), color: '#fff' }} />
              </Grid>
            ))}
          </Grid>
          <Typography variant="h6" component="p">
            Stats:
          </Typography>
          {pokemon.stats.map((stat) => (
            <Typography variant="body2" key={stat.stat.name} color="textSecondary">
              {stat.base_stat}
              {' '}
              {bull}
              {' '}
              {stat.stat.name}
            </Typography>
          ))}
        </CardContent>
        <CardActions>
          <Button size="small">Learn More</Button>
        </CardActions>
      </Box>
    </Card>
  )
})
export default PokemonCard
