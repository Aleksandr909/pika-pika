import Button from '@material-ui/core/Button'
import Chip from '@material-ui/core/Chip'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import MuiDialogContent from '@material-ui/core/DialogContent'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import Grid from '@material-ui/core/Grid'
import IconButton from '@material-ui/core/IconButton'
import {
  createStyles, makeStyles, Theme, withStyles, WithStyles,
} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import CloseIcon from '@material-ui/icons/Close'
import { observer } from 'mobx-react-lite'
import React from 'react'
import { getColor } from '../../core/functions'
import PokemonsStore from '../../stores/PokemonsStore'
import { Pokemon } from '../../stores/PokemonsStore/types'
import UsersStore from '../../stores/UsersStore'

const styles = (theme: Theme) => createStyles({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
})

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const {
    children, classes, onClose, ...other
  } = props
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent)

const PokemonDialog = observer(({ currentPokemon }: { currentPokemon: Pokemon }) => {
  const classes = useStyles()
  const { changeCurrentPokemon } = React.useContext(PokemonsStore)
  const { currentUser, favoriteHandler } = React.useContext(UsersStore)
  const isFavorite = !!currentUser?.favorite.includes(currentPokemon.name)
  const handleClose = () => {
    changeCurrentPokemon(null)
  }
  const bull = <span className={classes.bullet}>•</span>

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="dialog-title"
        open
        fullWidth
        maxWidth="xs"
      >
        <DialogTitle id="dialog-title" onClose={handleClose} classes={{ root: classes.title }}>
          {currentPokemon.name}
        </DialogTitle>
        <DialogContent dividers>
          <img
            src={currentPokemon.sprites.front_default}
            alt={currentPokemon.name}
            className={classes.img}
          />
          <Grid container spacing={1} className={classes.chipBlock}>
            {currentPokemon.types.map(({ type }) => (
              <Grid item key={type.name}>
                <Chip label={type.name} size="small" style={{ backgroundColor: getColor(type.name), color: '#fff' }} />
              </Grid>
            ))}
          </Grid>
          <Typography variant="h6" component="p">
            Abilities:
          </Typography>
          {currentPokemon.abilities.length > 0 ? currentPokemon.abilities.map((ability) => (
            <Typography variant="body2" key={ability.ability.name} color="textSecondary">
              {bull}
              {' '}
              {ability.ability.name}
            </Typography>
          )) : (
            <Typography variant="body2" color="textSecondary">
              Not Found
            </Typography>
          )}
          <br />
          <Typography variant="h6" component="p">
            Stats:
          </Typography>
          {currentPokemon.stats.length > 0 ? currentPokemon.stats.map((stat) => (
            <Typography variant="body2" key={stat.stat.name} color="textSecondary">
              {stat.base_stat}
              {' '}
              {bull}
              {' '}
              {stat.stat.name}
            </Typography>
          )) : (
            <Typography variant="body2" color="textSecondary">
              Not Found
            </Typography>
          )}
        </DialogContent>
        <DialogActions>
          <Button
            color="primary"
            variant={isFavorite ? 'contained' : 'outlined'}
            onClick={() => favoriteHandler(currentPokemon.name)}
          >
            Favorite
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
})

const useStyles = makeStyles({
  title: {
    textTransform: 'capitalize',
    '& h6': { fontSize: '1.75rem' },
  },
  img: {
    objectFit: 'contain',
    maxWidth: '100%',
  },
  name: {
    textTransform: 'capitalize',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  chipBlock: {
    margin: '16px 0',
  },
})

export default PokemonDialog
