import React from 'react'
import { Route, Redirect, useLocation } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import UsersStore from '../../stores/UsersStore'
import CustomProgress from '../atoms/CustomProgress'
import { getValueFromLocalStorage } from '../../core/utils/localStorage'
import history from '../../history'

type Props = {
  children: React.ReactNode;
  path: string;
  exact?: boolean
}

/**
 * Роут, проверяющий авторизован ли пользователь и если нет - перекидывает на страницу авторизации
 * @param children содержание роута для авторизировнных юзеров
 * @param path ссылка для авторизированных юзеров
 */
const PrivateRoute = observer((props: Props) => {
  const { loading, logoutSuccess } = React.useContext(UsersStore)
  const token = getValueFromLocalStorage('token')
  const locationData = useLocation().state as {from?: {[key:string]: any}}
  const historyPath = (locationData?.from as {pathname?:string})?.pathname

  React.useEffect(() => {
    if (logoutSuccess) history.push(historyPath || '/')
  }, [logoutSuccess, historyPath])

  if (loading) return <CustomProgress />

  return (
    <Route
      path={props.path}
      render={({ location }) => (token ? (
        props.children
      ) : (
        <Redirect to={{ pathname: '/sign-in', state: { from: location } }} />
      ))}
    />
  )
})

export default PrivateRoute
