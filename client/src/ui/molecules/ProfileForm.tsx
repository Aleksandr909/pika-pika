import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { observer } from 'mobx-react-lite'
import React from 'react'
import { useForm } from 'react-hook-form'
import useServerErrorsHandler from '../../core/hooks/useServerErrorsHandler'
import UsersStore from '../../stores/UsersStore'
import { User } from '../../stores/UsersStore/types'
import ChangePassword from './ChangePassword'

const ProfileForm = observer(() => {
  const {
    currentUser, pathcUser, serverErrors, loading,
  } = React.useContext(UsersStore)
  const {
    register, handleSubmit, errors, setError,
  } = useForm({ defaultValues: currentUser as User })
  useServerErrorsHandler(serverErrors, setError)

  const onSubmit = handleSubmit((newData) => {
    pathcUser(newData)
  })
  return (
    <form onSubmit={onSubmit}>
      <Grid container alignItems="flex-start" direction="row" spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <Grid container spacing={3} direction="column">
            <Grid item xs={12}>
              <TextField
                id="last_name"
                name="last_name"
                placeholder="Last name"
                variant="outlined"
                size="small"
                fullWidth
                autoComplete="family-name"
                error={!!errors.last_name}
                helperText={errors.last_name?.message}
                inputRef={register({ required: true })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="first_name"
                name="first_name"
                placeholder="First name"
                variant="outlined"
                size="small"
                autoComplete="given-name"
                fullWidth
                error={!!errors.first_name}
                helperText={errors.first_name?.message}
                inputRef={register({ required: true })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="middle_name"
                name="middle_name"
                placeholder="Middle name"
                variant="outlined"
                size="small"
                autoComplete="additional-name"
                fullWidth
                error={!!errors.middle_name}
                helperText={errors.middle_name?.message}
                inputRef={register({ required: true })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="email"
                name="email"
                placeholder="email"
                variant="outlined"
                autoComplete="email"
                size="small"
                fullWidth
                disabled
                error={!!errors.email}
                helperText={errors.email?.message}
                inputRef={register({ required: true })}
              />
            </Grid>
            <Grid item xs={12}>
              <ChangePassword />
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                type="submit"
                fullWidth
                color="primary"
                disabled={loading}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </form>
  )
})

export default ProfileForm
