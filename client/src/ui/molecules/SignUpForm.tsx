import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Link from '@material-ui/core/Link'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { observer } from 'mobx-react-lite'
import React from 'react'
import { useForm } from 'react-hook-form'
import { Link as RouterLink } from 'react-router-dom'
import useServerErrorsHandler from '../../core/hooks/useServerErrorsHandler'
import UsersStore from '../../stores/UsersStore'
import { User } from '../../stores/UsersStore/types'

const SignUpForm = observer(() => {
  const { createUser, serverErrors, loading } = React.useContext(UsersStore)
  const classes = useStyles()
  const {
    register, errors, handleSubmit, setError,
  } = useForm<User>()
  useServerErrorsHandler(serverErrors, setError)
  const onSubmit = handleSubmit((data) => {
    createUser(data)
  })
  return (
    <Grid container direction="column" alignItems="center" justify="center" spacing={2}>
      <Grid item xs={12} sm={6} md={4} className={classes.authBlock}>
        <form onSubmit={onSubmit}>
          <TextField
            name="last_name"
            placeholder="Surname"
            variant="outlined"
            margin="dense"
            fullWidth
            inputRef={register({ required: true })}
            error={!!errors.last_name}
            helperText={errors.last_name?.message}
          />
          <TextField
            name="first_name"
            placeholder="Name"
            variant="outlined"
            margin="dense"
            fullWidth
            inputRef={register({ required: true })}
            error={!!errors.first_name}
            helperText={errors.first_name?.message}
          />
          <TextField
            name="middle_name"
            placeholder="Middle name"
            variant="outlined"
            margin="dense"
            fullWidth
            inputRef={register({ required: true })}
            error={!!errors.middle_name}
            helperText={errors.middle_name?.message}
          />
          <TextField
            name="email"
            placeholder="Email"
            variant="outlined"
            margin="dense"
            fullWidth
            inputRef={register({ required: true })}
            error={!!errors.email}
            helperText={errors.email?.message}
          />
          <TextField
            name="password"
            placeholder="Password"
            variant="outlined"
            margin="dense"
            fullWidth
            type="password"
            autoComplete="new-password"
            inputRef={register({ required: true })}
            error={!!errors.password}
            helperText={errors.password?.message}
          />
          <Button fullWidth color="primary" variant="contained" type="submit" disabled={loading}>
            sign up
          </Button>
        </form>
      </Grid>
      <Grid item>
        <Grid container>
          <Typography variant="body1" color="textSecondary">
            Already registered?
          </Typography>
          <Link
            component={RouterLink}
            underline="always"
            to="/sign-in"
            className={classes.reg}
          >
            <Typography variant="body1">sign in</Typography>
          </Link>
        </Grid>
      </Grid>
    </Grid>
  )
})

const useStyles = makeStyles({
  root: { height: '100%', padding: 48 },
  reg: { paddingLeft: 8 },
  authBlock: { padding: 24 },
})

export default SignUpForm
