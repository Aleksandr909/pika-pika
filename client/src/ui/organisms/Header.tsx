import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Link from '@material-ui/core/Link'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Hidden from '@material-ui/core/Hidden'
import Box from '@material-ui/core/Box'
import { useLocation } from 'react-router'
import { ReactComponent as GitLogo } from '../../img/gitlab-icon-1-color-black-rgb.svg'
import { LOGO } from '../../constants/icons'
import UsersStore from '../../stores/UsersStore'
import LinkCustom from '../atoms/LinkCustom'
import UserMenu from './UserMenu'

const Header = () => {
  const { currentUser } = React.useContext(UsersStore)
  const location = useLocation()
  return (
    <div>
      <AppBar>
        <Toolbar disableGutters>
          <Container fixed>
            <Grid container justify="space-between" alignItems="center">
              <Grid item>
                <LinkCustom to="/">
                  <Grid container alignItems="center" spacing={2}>
                    <Grid item>
                      <img src={LOGO} height={34} alt="logo" />
                    </Grid>
                    <Hidden smDown>
                      <Grid item>
                        <Typography variant="h4">Pokedex</Typography>
                      </Grid>
                    </Hidden>
                  </Grid>
                </LinkCustom>
              </Grid>

              <Grid item>
                <Grid container alignItems="center">
                  {currentUser ? (
                    <UserMenu />
                  ) : (
                    <Grid item>
                      <Grid container spacing={2}>
                        <Grid item>
                          <LinkCustom to={{ pathname: '/sign-in', state: { from: location } }}>
                            <Button color="inherit">
                              sign-in
                            </Button>
                          </LinkCustom>
                        </Grid>
                        <Grid item>
                          <LinkCustom to={{ pathname: '/sign-up', state: { from: location } }}>
                            <Button color="inherit">
                              sign-up
                            </Button>
                          </LinkCustom>
                        </Grid>
                      </Grid>
                    </Grid>
                  )}
                  <Grid item>
                    <Box lineHeight={1} marginLeft={2} marginBottom={-0.5}>
                      <Link href="https://gitlab.com/Aleksandr909/pika-pika" color="inherit" target="_blank" rel="noopener noreferrer" aria-label="Gitlab">
                        <GitLogo height={48} fill="currentColor" />
                      </Link>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Container>
        </Toolbar>
      </AppBar>
      <Toolbar id="back-to-top-anchor" />
    </div>
  )
}

export default Header
