import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Fab from '@material-ui/core/Fab'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import { observer } from 'mobx-react-lite'
import UsersStore from '../../stores/UsersStore'
import ScrollTop from '../atoms/ScrollTop'
import Header from './Header'

interface Props {
  window?: () => Window;
  children: React.ReactElement;
}

const Layout = observer(({ children, ...props }: Props) => {
  const { getUserSelf, currentUser } = React.useContext(UsersStore)
  React.useEffect(() => {
    if (!currentUser) getUserSelf()
    // eslint-disable-next-line
  }, [getUserSelf])

  return (
    <>
      <CssBaseline />
      <Header />
      <div style={{ position: 'relative', paddingTop: 24 }}>
        {children}
      </div>
      <ScrollTop {...props}>
        <Fab color="primary" size="small" aria-label="scroll back to top">
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTop>
    </>
  )
})
export default Layout
