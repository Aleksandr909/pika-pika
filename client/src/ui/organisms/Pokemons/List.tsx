import Grid from '@material-ui/core/Grid'
import * as React from 'react'
import { Pokemon } from '../../../stores/PokemonsStore/types'
import PokemonCard from '../../molecules/PokemonCard'

type IListProps = {
  pokemons: Pokemon[]
}

const List: React.FunctionComponent<IListProps> = ({ pokemons }:IListProps) => (
  <Grid container spacing={3}>
    {pokemons.map((pokemon, index) => (
      <Grid item xs={12} sm={6} md={4} lg={3} key={pokemon.name}>
        <PokemonCard pokemon={pokemon} />
      </Grid>
    ))}
  </Grid>
)

export default List
