import { isWidthDown } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import TablePagination from '@material-ui/core/TablePagination'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import useWidth from '../../../core/hooks/useWidth'
import PokemonsStore from '../../../stores/PokemonsStore'
import TypesStore from '../../../stores/TypesStore'
import CustomProgress from '../../atoms/CustomProgress'
import Search from '../../atoms/Search'
import TypeAutocomplete from '../../atoms/TypesAutocomplete'
import PokemonDialog from '../../molecules/PokemonDialog'
import TablePaginationActions from '../../molecules/TablePaginationActions'
import List from './List'

const Pokemons: React.FunctionComponent = observer((p) => {
  const {
    getPokemons, pokemons, loading, total, currentPokemon, setName, name,
  } = React.useContext(PokemonsStore)

  const { currentType } = React.useContext(TypesStore)
  const pageWidth = useWidth()
  const [page, setPage] = React.useState(0)
  const [rowsPerPage, setRowsPerPage] = React.useState(10)
  React.useEffect(() => {
    setPage(0)
    getPokemons(0, rowsPerPage, currentType)
    // eslint-disable-next-line
  }, [getPokemons, rowsPerPage, currentType, name])

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    getPokemons(newPage, rowsPerPage, currentType)
    setPage(newPage)
  }
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10))
  }

  return (
    <>
      <Grid container alignItems="center" style={{ marginBottom: 16 }}>
        <Grid item xs={12} sm="auto">
          <TypeAutocomplete />
        </Grid>
        <Grid item xs={12} sm="auto">
          <Search onChange={(e: any) => setName(e.target.value)} value={name} />
        </Grid>
      </Grid>
      {loading ? <CustomProgress /> : (
        <>
          {pokemons && <List pokemons={pokemons} />}
          {total > rowsPerPage && (
            <Grid container justify={isWidthDown('md', pageWidth) ? 'flex-start' : 'flex-end'}>
              <TablePagination
                component="div"
                rowsPerPageOptions={[10, 20, 50]}
                colSpan={3}
                count={total}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { 'aria-label': 'rows per page' },
                  native: true,
                }}
                labelRowsPerPage={isWidthDown('md', pageWidth) ? '' : 'rows per page'}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              />
            </Grid>
          )}
        </>
      )}
      {currentPokemon && <PokemonDialog currentPokemon={currentPokemon} />}
    </>
  )
})

export default Pokemons
