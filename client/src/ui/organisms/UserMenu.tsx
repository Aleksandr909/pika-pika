import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { makeStyles, Theme } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { ExpandLess, ExpandMore } from '@material-ui/icons'
import { observer } from 'mobx-react-lite'
import React from 'react'
import { useLocation } from 'react-router-dom'
import { removeValueFromLocalStorage } from '../../core/utils/localStorage'
import history from '../../history'
import UsersStore from '../../stores/UsersStore'

const UserMenu = observer(() => {
  const { currentUser, logout } = React.useContext(UsersStore)
  const location = useLocation().pathname
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  const handleProfile = () => {
    setAnchorEl(null)
    history.push('/profile')
  }
  const handleFavorite = () => {
    setAnchorEl(null)
    history.push('/favorite')
  }
  const handleLogout = () => {
    logout()
    removeValueFromLocalStorage('token')
  }
  return (
    <>
      <Box
        aria-controls="user-menu"
        aria-haspopup="true"
        onClick={handleClick}
        className={classes.userBlock}
      >
        <Grid container alignItems="center">
          <Typography variant="h6">
            {currentUser?.last_name}
            {' '}
            {currentUser?.first_name && currentUser?.first_name?.slice(0, 1)}
            .
            {' '}
            {currentUser?.middle_name && currentUser?.middle_name?.slice(0, 1)}
            .
          </Typography>
          {anchorEl ? <ExpandLess /> : <ExpandMore />}
        </Grid>
      </Box>
      <Menu
        id="user-menu"
        anchorEl={anchorEl}
        keepMounted
        open={!!anchorEl}
        onClose={handleClose}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <MenuItem onClick={handleProfile} selected={location === '/profile'}>
          Profile
        </MenuItem>
        <MenuItem onClick={handleFavorite} selected={location === '/favorite'}>
          Favorite
        </MenuItem>
        <MenuItem onClick={handleLogout}>Logout</MenuItem>
      </Menu>
    </>
  )
})

const useStyles = makeStyles((theme: Theme) => ({
  userBlock: {
    '&:hover': { cursor: 'pointer' },
  },
}))

export default UserMenu
