type CustomConfig = {
    mongoUri: string;
    jwtSecret: string;
    googleClientId: string;
    googleClientSecret: string
    facebookClientId: string;
    facebookClientSecret: string
}

const { CUSTOM_CONFIG } = process.env

const customConfig: CustomConfig = CUSTOM_CONFIG ? JSON.parse(CUSTOM_CONFIG) : {
  mongoUri: '',
  jwtSecret: '',
  googleClientId: '',
  googleClientSecret: '',
  facebookClientId: '',
  facebookClientSecret: '',
}

export default customConfig
