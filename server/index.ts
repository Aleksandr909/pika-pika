require('dotenv-flow').config()

import { OptionsJson } from 'body-parser'
import passport = require('passport')
import express = require('express')
import config = require('config')
import mongoose = require('mongoose')
import path = require('path')
import http = require('http')
import customConfig from './customConfig'

const app = express()
const server = http.createServer(app)
mongoose.set('useFindAndModify', false)
app.use(express.json({ extended: true } as OptionsJson))
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', '*')
  res.header('Access-Control-Allow-Methods', '*')
  next()
})

app.use('/api/auth', require('./routes/auth.routes'))
app.use('/api/user', require('./routes/user.routes'))

app.use('/uploads', express.static(path.join(__dirname, '/uploads')))
app.use('/avatars', express.static(path.join(__dirname, '/avatars')))
app.use('/.well-known', express.static(path.join(__dirname, '/.well-known')))

app.use(passport.initialize())
passport.serializeUser((user: any, done) => {
  done(null, user)
})
passport.deserializeUser((user: any, done) => {
  done(null, user)
})
// app.use(passport.session())
app.use('/api/passport/google', require('./passport/google'))
app.use('/api/passport/facebook', require('./passport/facebook'))

const PORT = config.get('port') || 5000
if (process.env.NODE_ENV === 'production') {
  app.use('/', express.static(path.resolve(__dirname, '..', 'client', 'build')))

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'client', 'build', 'index.html'))
  })
}
const start = async () => {
  try {
    await mongoose.connect(customConfig?.mongoUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    })
    server.listen(PORT, () => console.log(`Listening on port ${PORT}`))
  } catch (e) {
    console.log('Server Error', e.message)
    process.exit(1)
  }
}

start()
