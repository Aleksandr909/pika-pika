import {
  Request, RequestHandler, Response,
} from 'express'
import jwt = require('jsonwebtoken')
import User from '../models/user'
import { User as UserTypes } from '../models/types'
import customConfig from '../customConfig'

export interface RequestWithCurrentUser extends Request {
  currentUser: UserTypes // or any other type
  [key:string]: any
}

// type AuthMiddlewareType =
//   (req: RequestWithCurrentUser, res: Response, next: () => void) => Promise<void | Response<any>>
type AuthMiddlewareType = RequestHandler<any, any, RequestWithCurrentUser, any>
const auth:AuthMiddlewareType = async (
  req,
  res: Response,
  next: () => void,
) => {
  if (req.method === 'OPTIONS') {
    return next()
  }

  try {
    const token = req.headers.authorization?.split(' ')[1] // "Bearer TOKEN"
    if (!token) {
      return res.status(401).json({ message: 'Нет авторизации' })
    }
    const decoded = jwt.verify(token, customConfig.jwtSecret) as {[key:string]: any}
    if (+new Date() / 1000 > decoded?.exp) {
      return res.status(401).json({ message: 'Время сессии завершилось' })
    }
    const user = await User.findById(decoded?.id)

    if (!user) {
      return res.status(400).json({ message: 'Пользователь не найден' })
    }
    req.body.currentUser = user._doc
    next()
  } catch (e) {
    res.status(401).json({ message: e.message })
  }
}

export default auth
