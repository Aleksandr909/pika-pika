import { Document, model, Schema } from 'mongoose'
import { User } from './types'

interface UserModel extends Document {
  _doc: User
}

const schema: Schema = new Schema({
  email: { type: String, unique: true },
  avatar_path: { type: String },
  password: { type: String, required: true },
  last_name: { type: String, required: true },
  middle_name: { type: String, required: true },
  first_name: { type: String, required: true },
  google_id: { type: String },
  favorite: [{ type: String }],
})

export default model<UserModel>('user', schema)
