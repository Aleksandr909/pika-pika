import { Request, Response, Router } from 'express'
import { Strategy as FacebookStrategy } from 'passport-facebook'
import passport = require('passport')
import config = require('config')
import jwt = require('jsonwebtoken')
import User from '../models/user'
import customConfig from '../customConfig'

const router = Router()

passport.use(new FacebookStrategy({
  clientID: customConfig.facebookClientId,
  clientSecret: customConfig.facebookClientSecret,
  callbackURL: `${config.get('baseUrl')}/api/passport/facebook/callback`,
  passReqToCallback: true,
},
(async (_request: any, _accessToken: any, _refreshToken: any, profile: any, done: any) => {
  try {
    const email = profile.emails[0].value
    const { familyName: last_name, givenName: first_name, middleName: middle_name } = profile.name
    const user = await User.findOne({ email: profile.emails[0].value })
    if (user?._id && user._doc) {
      done(null, user)
    } else {
      const newUser = new User({
        last_name,
        first_name,
        middle_name,
        facebook_id: profile.id,
        email,
      })
      done(null, newUser)
    }
  } catch (err) {
    done(err)
  }
})))

router.get('/',
  passport.authenticate('facebook', {
    scope: ['email', 'profile'],
  }))
router.get('/callback', (req: Request & { logIn: any }, res: Response, next) => {
  passport.authenticate('facebook', (err, user) => {
    if (err) { res.status(500).json({ message: err.message }) }
    const token = jwt.sign({ id: user._id }, customConfig.jwtSecret, {
      expiresIn: '2h',
    })
    req.logIn(user, (loginErr: any) => {
      if (loginErr) { return next(loginErr) }
      return res.redirect(`${config.get('clientUrl')}/auth/facebook/${token}`)
    })
  })(req, res, next)
})

module.exports = router
