import { Request, Response, Router } from 'express'
import { Strategy as GoogleStrategy } from 'passport-google-oauth2'
import passport = require('passport')
import config = require('config')
import jwt = require('jsonwebtoken')
import User from '../models/user'
import customConfig from '../customConfig'

const router = Router()

passport.use(new GoogleStrategy({
  clientID: customConfig.googleClientId,
  clientSecret: customConfig.googleClientSecret,
  callbackURL: `${config.get('baseUrl')}/api/passport/google/callback`,
  passReqToCallback: true,
},
(async (_request: any, _accessToken: any, _refreshToken: any, profile: any, done: any) => {
  try {
    const email = profile.emails[0].value
    const { family_name: last_name, given_name: first_name, id: google_id } = profile
    const user = await User.findOne({ email: profile.emails[0].value })
    if (user?._id && user._doc) {
      if (!user._doc.google_id) {
        user._doc.google_id = google_id
        user.save()
      }
      done(null, user)
    } else {
      const newUser = new User({
        last_name, first_name, google_id, email,
      })
      done(null, newUser)
    }
  } catch (err) {
    done(err)
  }
})))

router.get('/',
  passport.authenticate('google', {
    scope: ['email', 'profile'],
  }))
router.get('/callback', (req: Request & { logIn: any }, res: Response, next) => {
  passport.authenticate('google', (err, user) => {
    if (err) { res.status(500).json({ message: err.message }) }
    const token = jwt.sign({ id: user._id }, customConfig.jwtSecret, {
      expiresIn: '2h',
    })
    req.logIn(user, (loginErr: any) => {
      if (loginErr) { return next(loginErr) }
      return res.redirect(`${config.get('clientUrl')}/auth/google/${token}`)
    })
  })(req, res, next)
})

module.exports = router
