import { Router, Request, Response } from 'express'
import { check, validationResult } from 'express-validator'
import bcrypt = require('bcryptjs')
import jwt = require('jsonwebtoken')
import User from '../models/user'
import customConfig from '../customConfig'

const router = Router()

// /api/auth/register
router.post(
  '/register',
  [
    check('email', 'Некорректный email').isEmail(),
    check('password', 'Минимальная длина пароля 6 символов').isLength({
      min: 6,
    }),
    check('last_name', 'Это обязательное поле').notEmpty(),
    check('first_name', 'Это обязательное поле').notEmpty(),
    check('middle_name', 'Это обязательное поле').notEmpty(),
  ],
  async (req: Request, res: Response) => {
    try {
      const errors = validationResult(req)

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Некорректные данные при регистрации',
        })
      }

      const { email, password, ...otherData } = req.body

      const candidate = await User.findOne({ email })

      if (candidate) {
        return res
          .status(400)
          .json({ message: 'Такой пользователь уже существует' })
      }

      const hashedPassword = await bcrypt.hash(password, 12)
      const user = new User({
        email,
        password: hashedPassword,
        ...otherData,
      })

      await user.save()

      const token = jwt.sign({ id: user.id }, customConfig.jwtSecret, {
        expiresIn: '2h',
      })

      res.status(201).json({
        token,
        id: user.id,
        email,
        ...otherData,
      })
    } catch (e) {
      res.status(500).json({
        message: e.message || 'Что-то пошло не так, попробуйте снова',
      })
    }
  },
)

// /api/auth/login
router.post(
  '/login',
  [
    check('email', 'Введите корректный email')
      .normalizeEmail()
      .isEmail(),
    check('password', 'Введите пароль').exists(),
  ],
  async (req: Request, res: Response) => {
    try {
      const errors = validationResult(req)

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Некорректные данные при входе в систему',
        })
      }

      const { email, password } = req.body

      const user = await User.findOne({ email })

      if (!user) {
        return res.status(400).json({ email: 'Пользователь не найден' })
      }

      const isMatch = await bcrypt.compare(password, user._doc.password)

      if (!isMatch) {
        return res.status(400).json({ email: 'Неверный email или пароль' })
      }

      const token = jwt.sign({ id: user.id }, customConfig.jwtSecret, {
        expiresIn: '2h',
      })

      const { ...userData } = user._doc

      res.json({ token, ...userData })
    } catch (e) {
      res.status(500).json({
        message: e.message || 'Что-то пошло не так, попробуйте снова',
      })
    }
  },
)

module.exports = router
