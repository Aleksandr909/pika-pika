import axios from 'axios'
import config = require('config')
import bcrypt = require('bcryptjs')
import { Router, Response } from 'express'
import auth, { RequestWithCurrentUser } from '../middleware/auth.middleware'
import User from '../models/user'

const router = Router()

interface IFavoriteTypes {
  type: { name: string }
}

// /api/user/user-self
router.get('/user-self', auth, async (req, res: Response) => {
  try {
    res.json(req.body.currentUser)
  } catch (e) {
    res
      .status(500)
      .json({ message: e.message || 'Что-то пошло не так, попробуйте снова' })
  }
})

// /api/user/user-self
router.patch('/user-self', auth, async (req, res: Response) => {
  try {
    const { currentUser, ...data } = req.body
    const user = await User.findByIdAndUpdate(
      currentUser._id,
      { ...data },
      { new: true },
    )
    res.json(user)
  } catch (e) {
    res
      .status(500)
      .json({ message: e.message || 'Что-то пошло не так, попробуйте снова' })
  }
})

// /api/user/change-password
router.patch('/change-password', auth, async (req, res: Response) => {
  try {
    const { old_password, new_password, currentUser } = req.body
    const isTrue = await bcrypt.compare(old_password, currentUser.password)
    if (isTrue) {
      const hashedPassword = await bcrypt.hash(new_password, 12)
      const user = await User.findByIdAndUpdate(
        currentUser._id,
        { password: hashedPassword },
        { new: true },
      )
      res.json(user)
    } else {
      res
        .status(500)
        .json({ old_password: 'Неверный пароль' })
    }
  } catch (e) {
    res
      .status(500)
      .json({ message: e.message || 'Что-то пошло не так, попробуйте снова' })
  }
})

// /api/user/favorite
router.patch('/favorite', auth, async (req, res: Response) => {
  try {
    const { currentUser, name } = req.body
    const favoriteIndex = currentUser.favorite.findIndex((elem) => elem === name)
    const userFavorites = [...currentUser.favorite]
    if (favoriteIndex === -1) {
      userFavorites.push(req.body.name)
    } else {
      userFavorites.splice(favoriteIndex, 1)
    }
    await User.findByIdAndUpdate(
      currentUser._id,
      { favorite: userFavorites },
    )
    res.json()
  } catch (e) {
    res
      .status(500)
      .json({ message: e.message || 'Что-то пошло не так, попробуйте снова' })
  }
})

// /api/user/favorite/?page&size&type&name
router.get('/favorite', auth, async (req, res: Response) => {
  try {
    const page = +(req.query?.page || 0)
    const size = +(req.query?.size || 10)
    const { type, name } = req.query
    const userFavorites = [...req.body.currentUser.favorite]
    // .slice(page * size, (page + 1) * size)
    const userFavoritesFull = []
    for (const favorite of userFavorites) {
      const favoriteFull = await axios.get(`${config.get('pokeapiUrl')}/pokemon/${favorite}`)
      userFavoritesFull.push(favoriteFull.data)
    }
    let pokemonsFull = []
    let total = 0
    if (type && name) {
      const filteredItems = userFavoritesFull.filter(
        (favorite) => !!favorite.types.find(
          (favoriteType: IFavoriteTypes) => favoriteType.type.name === type,
        ),
      )
      const neededItem = filteredItems.find((elem) => elem.name === name)
      if (neededItem) {
        pokemonsFull.push(neededItem)
        total = 1
      }
    } else if (type) {
      const filteredItems = userFavoritesFull.filter(
        (favorite) => !!favorite.types.find(
          (favoriteType: IFavoriteTypes) => favoriteType.type.name === type,
        ),
      )
      pokemonsFull.push(...filteredItems)
      total = filteredItems.length
    } else if (name) {
      const neededItem = userFavoritesFull.find((elem) => elem.name === name)
      if (neededItem) {
        pokemonsFull.push(neededItem)
        total = 1
      }
    } else {
      pokemonsFull.push(...userFavoritesFull)
      total = userFavoritesFull.length
    }
    pokemonsFull = pokemonsFull.slice(page * size, (page + 1) * size)

    res.json({ results: pokemonsFull, count: total })
  } catch (e) {
    res
      .status(500)
      .json({ message: e.message || 'Что-то пошло не так, попробуйте снова' })
  }
})

module.exports = router
